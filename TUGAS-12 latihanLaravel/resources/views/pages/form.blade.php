<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form</title>
</head>
<body>
<h1>Buat Account Baru</h1>
<h3>Sign Up Form</h3>
<form action="welcome.html" method="get">
    <label>First name</label><br><br>
    <input type="text" name="name"><br><br>
    <label>Last name</label><br><br>
    <input type="text" name="name"><br><br>
    <label>Gender</label><br><br>
    <input type="radio">Male<br>
    <input type="radio">Female<br>
    <input type="radio">Other<br><br>
    <label>Nationality</label><br><br>
    <select name="Nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Inggris">Ingris</option>
        <option value="Other">Other</option>
    </select><br><br>
    <label for="Language Spoken">Language Spoken</label><br><br>
    <input type="checkbox" id="Language Spoken">Bahasa Indonesia<br>
    <input type="checkbox" id="Language Spoken">Bahasa Inggris<br>
    <input type="checkbox" id="Language Spoken">Other<br><br>
    <label for="Bio">Bio</label><br><br>
    <textarea id="Bio" rows="12" cols="50"></textarea><br><br>
    <button>Sign Up</button>

</form>
</body>
</html>
